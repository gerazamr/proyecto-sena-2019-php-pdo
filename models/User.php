<?php
require_once 'controllers/traits/helperTrait.php';

class User
{
	
    use helperTrait; //Inyecta los metodos del Trait para ser usados como métodos de la clase
	private $pdo;    
    private $id;
    private $name;
    private $email;
    private $phone;

	public function __CONSTRUCT()
	{
		try {
			$this->pdo = Database::connect();     
		}
		catch(Exception $e)	{
			die($e->getMessage());
		}
	}

	
	public function login($email, $pass){

		try {
			$query = $this->pdo->prepare("SELECT * FROM user WHERE email = ? "); // Se crea la consulta	          
			$query->execute( array($email) ); // se reemplazan valores
			$user = $query->fetch(PDO::FETCH_OBJ); // devolvemos el resultado como un objeto
			
			if($user){ // si se encontró el correo se verifica el password del usuario y se crea la sesión

				if (password_verify($pass, $user->password) ){					
					$_SESSION['user_id'] = $user->id;
					$_SESSION['user_name'] = $user->name;
					$_SESSION['user_last_name'] = $user->last_name;
					$_SESSION['user_email'] = $user->email;
					return true;

				} else {
					return false;
				}

			}else{
				return false;
			}
		

		} catch (Exception $e) {
			die($e->getMessage());
		}

	}


	public function all()
	{
		try	{
			$query = $this->pdo->prepare("SELECT * FROM user");
			$query->execute();
			return $query->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	
		public function store($data)
	{
		try {
			$sql = "INSERT INTO user (name,last_name,email,phone,password) 
			        VALUES (?, ?, ?, ?, ?)";

			$this->pdo->prepare($sql)
			     ->execute(
					array(
						$data['name'],
						$data['last_name'], 
	                    $data['email'],
	                    $data['phone'],
	                    $this->encrypt_string($data['password'])
	                )
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	

	public function show($id)
	{
		try {
			$query = $this->pdo->prepare("SELECT * FROM user WHERE id = ?");	          
			$query->execute(array($id));
			return $query->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


	public function update($data)
	{
			$query = $this->pdo->prepare("SELECT password FROM user WHERE id = ?");	          
			$query->execute(array($data["id"]));
			$current_password = $query->fetch(PDO::FETCH_OBJ);
			
			if ($data["password"]!=$current_password->password){	
				//Si el password cambió se realiza su actualización
				$sql = "UPDATE user SET  
						password = ?					
				    	WHERE id = ?";

				$this->pdo->prepare($sql)
					 ->execute(
						array(
							$this->encrypt_string($data['password']), 
							$data["id"]
						)
					);
							}
							
		
		try {
			$sql = "UPDATE user SET 
						name      = ?,
						last_name = ?,
						email     = ?, 
						phone     = ?				
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
				    	$data["name"], 
				    	$data["last_name"], 
                        $data["email"],                        
                        $data["phone"],                        
                        $data["id"]
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


	
	
	public function delete($id)
	{ 
		try {
			$query = $this->pdo->prepare("DELETE FROM user WHERE id = ?");
			$query->execute(array($id));
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
}