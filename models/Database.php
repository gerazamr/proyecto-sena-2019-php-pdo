<?php
class Database
{
    

    public static function connect()
    {
        $host = 'localhost';
        $db = 'proyecto_tecnico';
        $user = 'root';
        $pass = '';//reemplazar por la contraseña de mysql de su equipo

        $pdo = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $pass, array());
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
        return $pdo;
    }
}