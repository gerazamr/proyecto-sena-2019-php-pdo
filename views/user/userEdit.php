<div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-user"></i>
                    Creación de Usuarios
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        
                        
                            <form id="frm-alumno" action="" method="post">
                            
                              <input type="hidden" name="c" value="user"> <?php /* El formulario llamará el controlador Auth */ ?>
                              <input type="hidden" name="m" value="update"> <?php /* Y su método Login */ ?>
                              <input type="hidden" name="id" value="<?php echo $item->id?>"> <?php /* Y su método Login */ ?>
                              
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="name" class="form-control" value="<?php echo $item->name ?>" placeholder="Ingrese su Nombre" required>
                                </div>
                                
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" name="last_name" class="form-control" value="<?php echo $item->last_name ?>" placeholder="Ingrese su Apellido" required>
                                </div>
                                
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control" value="<?php echo $item->email ?>" placeholder="Ingrese su Email" required>
                                </div>
                                
                                <div class="form-group">
                                    <label>Telefono</label>
                                    <input type="phone" name="phone" class="form-control" value="<?php echo $item->phone ?>" placeholder="Ingrese su Telefono" required>
                                </div> 
                                
                                <div class="form-group">
                                    <label>Contrase&ntilde;a</label>
                                    <input type="password" name="password" class="form-control" value="<?php echo $item->password ?>" placeholder="Ingrese su Contrase&ntilde;a" required>
                                </div> 
                                
                                <hr />
                                
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Actualizar</button>
                                    <button type="button" onclick="window.location.replace('?c=user&m-index');"  class="btn btn-secondary">Cancelar</button>
                                </div>
                            </form>
                        
                        
                        
                        
                    </div>
                </div>
                <div class="card-footer small text-muted">Actualizado Ayer a las 11:59 PM</div>
            </div>



