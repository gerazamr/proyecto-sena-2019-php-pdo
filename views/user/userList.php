<div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Listado de Usuarios
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach( $list as $item) { // $list es la variable que viene desde el controlador ?>
                                <tr>
                                    <td> <?php echo $item->id ?> </td>
                                    <td> <?php echo $item->name ?> </td>
                                    <td> <?php echo $item->last_name ?> </td>
                                    <td> <?php echo $item->email ?> </td>
                                    <td> <?php echo $item->phone ?> </td>
                                    <td> 
                                        <a href="index.php?c=user&m=show&id=<?php echo $item->id;?>" 
										    title="Editar"
										    class='btn btn-success btn-sm'>
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        
                                        <?php if($item->id != $_SESSION["user_id"]){?>
                                        <a href="javascript:void(0)"
                                        	onclick="if(confirm('Esta seguro de eliminar a este usuario?')){ window.location.replace('index.php?c=user&m=delete&id=<?php echo $item->id;?>'); }else{ return false; }" 
										    title="Eliminar"
										    class='btn btn-danger btn-sm'>
                                            <i class="fas fa-trash"></i>
                                        </a>
                                        <?php }?>
                                  </td>
                                </tr>
                                <?php  }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">Actualizado Ayer a las 11:59 PM</div>
            </div>