<div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-user"></i>
                    Creación de Usuarios
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        
                        
                            <form id="frm-alumno" action="" method="post">
                            
                              <input type="hidden" name="c" value="user"> <?php /* El formulario llamará el controlador Auth */ ?>
                              <input type="hidden" name="m" value="store"> <?php /* Y su método Login */ ?>
                              
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="name" class="form-control" placeholder="Ingrese su Nombre" required>
                                </div>
                                
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" name="last_name" class="form-control" placeholder="Ingrese su Apellido" required>
                                </div>
                                
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control" placeholder="Ingrese su Email" required>
                                </div>
                                
                                <div class="form-group">
                                    <label>Telefono</label>
                                    <input type="phone" name="phone" class="form-control" placeholder="Ingrese su Telefono" required>
                                </div>   
                                
                                <div class="form-group">
                                    <label>Contrase&ntilde;a</label>
                                    <input type="password" name="password" class="form-control" value="" placeholder="Ingrese su Contrase&ntilde;a" required>
                                </div> 
                                
                                
                                <hr />
                                
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <button type="button" onclick="window.location.replace('?c=user&m-index');"  class="btn btn-secondary">Cancelar</button>
                                </div>
                            </form>
                        
                        
                        
                        
                    </div>
                </div>
                <div class="card-footer small text-muted">Actualizado Ayer a las 11:59 PM</div>
            </div>



