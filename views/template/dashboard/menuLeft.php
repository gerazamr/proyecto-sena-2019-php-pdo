<ul class="sidebar navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="index.php?c=dashboard&m=index">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Panel de control</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa fa-child"></i>
                <span>Usuarios</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <h6 class="dropdown-header">Opciones de Usuario:</h6>
                <a class="dropdown-item" href="index.php?c=user&m=create">Crear</a>
                <a class="dropdown-item" href="index.php?c=user&m=index">Listado</a>
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">Otras Opciones:</h6>
                <a class="dropdown-item" href="index.php?c=profile&m=create">Crear Perfiles</a>
                <a class="dropdown-item" href="index.php?c=profile&m=index">Listar Perfiles</a>
            </div>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa fa-archive"></i>
                <span>Productos</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <h6 class="dropdown-header">Opciones de Productos:</h6>
                <a class="dropdown-item" href="index.php?c=product&m=create">Crear</a>
                <a class="dropdown-item" href="index.php?c=product&m=index">Listado</a>
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">Otras Opciones:</h6>
                <a class="dropdown-item" href="index.php?c=product&m=in">Ingreso Productos</a>
                <a class="dropdown-item" href="index.php?c=product&m=inventory">Inventario</a>
            </div>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa fa-cart-arrow-down"></i>
                <span>Ventas</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                <h6 class="dropdown-header">Opciones de Ventas:</h6>
                <a class="dropdown-item" href="index.php?c=sales&m=create">Facturar</a>
                <a class="dropdown-item" href="index.php?c=sales&m=index">Ver Historico</a>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="index.php?c=reports&m=index">
            <i class="fas fa-chart-bar"></i>
                <span>Reportes</span></a>
        </li>

    </ul>