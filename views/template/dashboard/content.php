<?php
require_once 'views/template/dashboard/header.php';
require_once 'views/template/dashboard/menuTop.php';
?>

<div id="wrapper"> <!-- Este div se cierra en el archivo footer -->

<!-- Menu Left -->
<?php require_once('views/template/dashboard/menuLeft.php')?>
<!-- Menu Left -->

<div id="content-wrapper"> <!-- Este div se cierra en el archivo footer -->

    
    
    <div class="container-fluid">

    
            <!-- Miga de Pan-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo $section_Link?>"><?php echo $section_name?></a>
                </li>
                <li class="breadcrumb-item active"><?php echo $section_method?></li>
            </ol>
  
            <?php require_once('views/'.$show_section)?>


    </div>
    <!-- /.container-fluid --> 
    
<?php    
    require_once 'views/template/dashboard/footer.php';//template   
    ?>