<?php if(isset($_REQUEST["message"]) && !empty($_REQUEST["message"])) {
        // este fragmento de código muestra los errores enviados en la variable message ?>

        <div style="font-size: 0.8rem" class="text-center alert alert-<?php echo (isset($_REQUEST["type"]))?$_REQUEST["type"]:'primary'; ?>" role="alert"> 
        <?php  //type define el color del mensaje según los estilos de bootstrap (primary, secondary, success, danger, warning, info, light, dark) ?>
          <?php 
          switch($_REQUEST["message"]){
            case 'page_not_found':
              echo 'No se ha encontrado el recurso solicitado';
            break;
            case 'session_expired':
              echo 'Tu sesión ha expirado. Inicia sesión nuevamente.';
            break;
            case 'user_not_found':
              echo 'Error de autenticación';
            break;
            case 'session_closed':
              echo 'Sesión cerrada con éxito';
            break;
            default:
              echo 'Error';
            break;
            }          
          ?>
        </div>
      <?php  } //errores?>