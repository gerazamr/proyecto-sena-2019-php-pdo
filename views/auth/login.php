
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-center">Proyecto SENA - Iniciar Sesión</div>
      <div class="card-body">


     <?php require('views/template/errorHandler.php'); ?>


        <form method="post" action="index.php">

          <input type="hidden" name="c" value="auth"> <?php /* El formulario llamará el controlador Auth */ ?>
          <input type="hidden" name="m" value="login"> <?php /* Y su método Login */ ?>

          <div class="form-group">
            <div class="form-label-group">
              <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required="required" autofocus="autofocus">
              <label for="inputEmail">Email</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required="required">
              <label for="inputPassword">Contraseña</label>
            </div>
          </div>
          <div class="form-group">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="remember-me">
                Recordar Contraseña
              </label>
            </div>
          </div>
          <button type="submit" class="btn btn-primary btn-block" href="index.html">Iniciar sesión</button>
        </form>


        <div class="text-center">
          <a class="d-block small" href="?c=auth&m=forgot_password">Olvidaste tu contraseña?</a>
        </div>
      </div>
    </div>
  </div>
