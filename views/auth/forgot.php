    <div class="container">
        <div class="card card-login mx-auto mt-5">
          <div class="card-header text-center">Proyecto SENA - Recuperación de contraseña</div>
          <div class="card-body">

            <div class="text-center mb-4">
              <h4>Olvidaste tu contraseña?</h4>
              <p> Ingresa tu correo electrónico y te enviaremos las instrucciones para la recuperación de contraseña.</p>
            </div>

            <form method="post" action="index.php">

              <input type="hidden" name="c" value="auth"> <?php /* El formulario llamará el controlador Auth */ ?>
              <input type="hidden" name="m" value="password_recovery"> <?php /* Y su método password_recovery */ ?>

              <div class="form-group">
                <div class="form-label-group">
                  <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Ingrese su email" required="required" autofocus="autofocus">
                  <label for="inputEmail">Ingresa tu Email</label>
                </div>
              </div>
              <button class="btn btn-primary btn-block" type="submit">Recuperar contraseña</button>
          </form>
            <div class="text-center">
              <a class="d-block small" href="index.php">Volver</a>
            </div>

          </div>
        </div>
      </div>
  
  
  creamos 19