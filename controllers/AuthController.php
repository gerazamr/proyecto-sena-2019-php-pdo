<?php
require_once 'traits/helperTrait.php';
require_once 'models/User.php';

class AuthController
{   
    use helperTrait; //Inyecta los metodos del Trait para ser usados como métodos de la clase
    private $model;
    
    public function __CONSTRUCT()
    {
        $this->model = new User();
    }
    
    public function index()
    {
        session_destroy();
        require_once 'views/template/login/header.php';
        require_once 'views/auth/login.php';  
        require_once 'views/template/login/footer.php';    
    }
    

    public function login(){
        $email = filter_var($_POST["inputEmail"], FILTER_SANITIZE_EMAIL); //Elimina todos los caracteres menos letras, dígitos y !#$%&'*+-=?^_`{|}~@.[]. (Evita inyeccón SQL)
        $pass = $_POST["inputPassword"];

        $user = new User();  // Creamos un objeto del modelo User para realizar la autenticación  
        $response = $user->login($email, $pass);

        if($response){
            //Si la autenticación es exitosa redirige al panel principal
            header("Location: index.php?c=dashboard&m=index");
        }else{
            // Si la autenticación falla se muestrar error
            header("Location: index.php?message=user_not_found&type=danger");
        }

    }

    public function forgot_password(){
        session_destroy();
        require_once 'views/template/login/header.php';
        require_once 'views/auth/forgot.php';  
        require_once 'views/template/login/footer.php';    
    }

    
    public function password_recovery(){
      exit("Envio de email en desarrollo");
    }

    
 
}