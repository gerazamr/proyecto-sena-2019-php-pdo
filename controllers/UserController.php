<?php
require_once 'traits/helperTrait.php';
require_once 'models/User.php';

class UserController
{   
    use helperTrait; //Inyecta los metodos del Trait para ser usados como métodos de la clase
    private $model;
    
    public function __CONSTRUCT()
    {
        if(!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])){
            header("Location: index.php?message=session_expired&type=info");
           }

        $this->model = new User();
    }
    
    public function index()
    {
        
        $list = $this->model->all(); // Consulta al modelo el metodo all y devuelve su resultado para ser usado en la vista

        $show_section = 'user/userList.php'; // vista donde se muestra la tabla (Carpeta Views)

        $section_name = 'Usuarios';
        $section_Link = 'index.php?c=user&m=index';
        $section_method = 'Listado';
        require_once 'views/template/dashboard/content.php';//template   
    }
    
    public function create() 
    {

        $show_section = 'user/userCreate.php'; // vista donde se muestra la tabla (Carpeta Views)
        $section_name = 'Usuarios';
        $section_Link = 'index.php?c=user&m=index';
        $section_method = 'Crear';
        require_once 'views/template/dashboard/content.php';//template  
    }
    
    public function store() 
    {
        $user = new User();
        $user->store($_POST);               
        header('Location: index.php?c=user&m=index');
    }   
	  
    public function show() 
    {
		$item = $this->model->show(filter_var($_REQUEST['id'], FILTER_VALIDATE_INT));
		if(!empty($item)){
			$show_section = 'user/userEdit.php'; // vista donde se muestra la información (Carpeta Views)
			$section_name = 'Usuarios';
			$section_Link = 'index.php?c=user&m=index';
			$section_method = 'Actualizar';
			require_once 'views/template/dashboard/content.php';//template
			
				}else{
		       	 header('Location: index.php?c=user&m=index');	
			}
          
    }
	
	  
    public function update() 
    {
        $user = new User();
        $user->update($_POST);               
        header('Location: index.php?c=user&m=index');
    } 
	
	  
    public function delete() 
    {
        $user = new User();
        $user->delete($_REQUEST["id"]);               
        header('Location: index.php?c=user&m=index');
    } 
}