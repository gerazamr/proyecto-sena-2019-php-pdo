 <?php 
// Archivo con funciones globales para el proyecto

function project_state($state = 'production'){

        if($state == 'production'){
            ini_set('display_errors', 0);
            ini_set('display_startup_errors', 0);
            error_reporting(0);
        }else{
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
    }

function set_time_zone($zone = 'America/Bogota'){
    

        date_default_timezone_set($zone);
    }
     


    