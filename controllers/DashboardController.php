<?php
require_once 'traits/helperTrait.php';
require_once 'models/User.php';

class DashboardController
{   
    use helperTrait; //Inyecta los metodos del Trait para ser usados como métodos de la clase
    private $model;
    
    public function __CONSTRUCT()
    {
       if(!isset($_SESSION['user_id']) || empty($_SESSION['user_id'])){
        header("Location: index.php?message=session_expired&type=info");
       }
    }
    
    public function index()
    {
        $show_section = 'dashboard/dashboard.php';
        $section_name = 'Panel de Control';
        $section_Link = 'index.php?c=dashboard&m=index';
        $section_method = 'Resumen';
        require_once 'views/template/dashboard/content.php';//template   
    }
   

}