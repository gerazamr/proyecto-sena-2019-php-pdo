<?php
    session_start(); //Se inicializan las variables de sesión

    require_once "controllers/traits/helpers.php";
    project_state('development'); 
    // Usamos la función del archivo helper proyect_state() para mostrar todos los errores que puedan surgir en desarrollo ('development'), 
    //en la opción 'production' no se muestran los errores al usuario final
    set_time_zone('America/Bogota');  // Establece zona horaria


    require_once 'models/Database.php';// Inicializa BD

    $controller = 'Auth'.'Controller';


    // Toda esta lógica hara el papel de un FrontController
    if(!isset($_REQUEST['c'])) {

        //Si en la URL no hay una petición a un controlador, por defecto se llamará el AuthController y el Netodo Index (Login Form)
        require_once "controllers/$controller".".php";
        $controller = ucwords($controller);
        $controller = new $controller;
        $controller->index();    // Método Index

    } else {

        // Obtenemos el método que queremos cargar
        $controller = ucwords($_REQUEST['c']).'Controller';
        $method = isset($_REQUEST['m']) ? $_REQUEST['m'] : 'index';
            

            // Verificamos si el archivo, el controlador y el método del controlador existen
                if(file_exists("controllers/$controller".".php")){
                    require_once "controllers/$controller".".php";
                }else{
                    header("Location: index.php?message=page_not_found&type=danger");
                }

                if(class_exists($controller)){
                    //Instanciamos el controlador
                    $controller = new $controller ; 
                }else{
                    header("Location: index.php?message=page_not_found&type=danger");
                }

                if(method_exists($controller, $method)){
                    //llamamos el método correspondiente
                    call_user_func(array($controller, $method));   
                }else{
                    header("Location: index.php?message=page_not_found&type=danger");
                }

        }